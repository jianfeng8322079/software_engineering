#include<iostream>
 #include<cstdlib>
 using namespace std;
 bool g_InvalidInput = false;
 int getMaxSum(int *a, int len, int start, int end)
 {
 if (a == NULL || len <= 0)
 {
    g_InvalidInput = true;
    return 0;
 }  
 g_InvalidInput = false;
 start = 0;
 end = 0;
 if (1 == len)
    return a[0];
 int curSum = a[0];           //当前子数组和  
 int maxSum = curSum;         //子数组最大和 
 for (int i = 1; i < len; i++)
 {
    if (curSum > 0)
        curSum += a[i];
    else
       if (a[i] > curSum)    //确定子数组起始位置
       {
            start = i;
            curSum = a[i];    //即使数组全为负，也能得到最大值
        } 
   if (curSum > maxSum)
   {
        maxSum = curSum;
        end = i;
   }
 }
 return maxSum;
}
int getstart(int *a, int len, int start, int end)
{
if (a == NULL || len <= 0)
{
    g_InvalidInput = true;
    return 0;
}  
g_InvalidInput = false;
start = 0;
end = 0;
if (1 == len)
    return a[0];
int curSum = a[0];           //当前子数组和  
int maxSum = curSum;         //子数组最大和 
for (int i = 1; i < len; i++)
{
    if (curSum > 0)
        curSum += a[i];
    else
        if (a[i] > curSum)    //确定子数组起始位置
        {
            start = i;
            curSum = a[i];    //即使数组全为负，也能得到最大值
        } 
    if (curSum > maxSum)
    {
        maxSum = curSum;
        end = i;
    }
    
}
return start;
}
int getend(int *a, int len, int start, int end)
{
if (a == NULL || len <= 0)
{
    g_InvalidInput = true;
    return 0;
}  
g_InvalidInput = false;
start = 0;
end = 0;
if (1 == len)
    return a[0];
int curSum = a[0];           //当前子数组和  
int maxSum = curSum;         //子数组最大和 
for (int i = 1; i < len; i++)
{
    if (curSum > 0)
        curSum += a[i];
    else
        if (a[i] > curSum)    //确定子数组起始位置
        {
            start = i;
            curSum = a[i];    //即使数组全为负，也能得到最大值
        } 
    if (curSum > maxSum)
    {
        maxSum = curSum;
        end = i;
    }
    
}
return end;
}
int main(){
int smax,start,send;
int a[]={-32,-10,33,-23,32,-12,41,-12,1,3,5,-98,70,-21,10,-9,61};
smax=getMaxSum(a,17,0,16);
cout<<"最大的子数组和为："<<smax<<endl;
start=getstart(a,17,0,16);
cout<<"最大的子数组开始下标为："<<start<<endl;
send=getend(a,17,0,16);
cout<<"最大的子数组结束下标为："<<send<<endl;
system("pause");
return 0;
 }